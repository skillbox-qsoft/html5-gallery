<?php
// CURRENT CONFIG
$maxSize = 5 * (1024 ** 2);
$maxNumber = 5;
$types = ['image/jpeg', 'image/png', 'image/gif', 'image/bmp'];
$extensions = 'jpg,jpeg,png,gif,bmp';

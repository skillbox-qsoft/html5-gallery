<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('log_errors', TRUE);
ini_set('display_startup_errors', TRUE);
include 'config.php';
if (!file_exists('upload')) {
    mkdir('upload');
    chmod('upload', 0777);
}
$dir = 'upload';
$msgBox = '';
if (isset($_POST['submit'])) {
    $countFiles = count($_FILES['file']['name']);
    for ($i = 0; $i < $countFiles; $i++) {
        if ($countFiles <= $maxNumber) {
            $tmp = $_FILES['file']['tmp_name'][$i];
            $type = mime_content_type($tmp);
            if (in_array($type, $types)) {
                $img = $_FILES['file']['name'][$i];
                move_uploaded_file($tmp, $dir.'/'.$img);
                chmod($dir.'/'.$img, 0777);
                $msgBox = 'Файлы успешно загружены';
            } else {
                $msgBox = 'Неправильный тип загружаемого файла';
            }
        } elseif ($countFiles > $maxNumber) {
            $msgBox = 'Превышено максимальное количество загружаемых файлов';
        }
    }
}
if (isset($_POST['delete'])) {
    foreach ($_POST['delete'] ?? [] as $img) {
        if (file_exists($img) && realpath($dir)) {
            unlink($img);
            $msgBox = 'Файлы успешно удалены';
        } else {
            $msgBox = 'Выбранный файл не существует';
        }
    }
}
$list = str_replace($dir.'/','',(glob($dir.'/*.{'.$extensions.'}', GLOB_BRACE)));
?>
<html>
<head>
<title>Загрузка изображений</title>
<link rel="shortcut icon" href="/favicon.png?rev=<?=time();?>" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/style.css?rev=<?=time();?>">
</head>
<body>
<h1><img class="hover" height="48px" src="/favicon.png?rev=<?=time();?>">Загрузка изображений</h1>
<form method="POST" enctype="multipart/form-data">
    <input type="file" name="file[]" id="file" accept="image/jpeg,image/png,image/gif" multiple>
    <input type="submit" name="submit" value="Загрузить">
</form>
<form method="POST" enctype="multipart/form-data">
    <input type="submit" value="Удалить выделенные изображения">
    <br>
    <font><?=$msgBox;?><font>
    <br>
    <?php
    foreach ($list as $key=>$value) {
        $filename = $dir.'/'.$value;
        ?>
        <a href="<?=$filename;?>">
            <img class="hover" width="15%" alt="<?=$filename;?>" src="<?=$filename;?>?rev=<?=time();?>" title="<?=$filename;?>">
        </a>
        <input type="checkbox" name="delete[]" value="<?=$filename;?>">
    <?php
    } ?>
</form>
</body>
</html>
